---
name: Rinita Gulliani
talks:
- An Introduction to Recursion
---

Rinita Gulliani is a software developer, speaker, and tech blogger. She’s worked with many programming languages including Python, PHP, Java, C#, SQL, and many more. She is active with several women in tech groups in her local community including Women Who Code and Girl Develop It. She is a member of the online Ela community. She loves speaking and blogging about technology because they are both great ways to share knowledge.
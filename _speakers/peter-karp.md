---
name: Peter Karp
talks:
- Automated Video Editing with Stitcher
---

Peter Karp is a Software Engineering Manager for Production Tools at BuzzFeed. He is a co-organizer for DjangoNYC.  Peter has given talks on various topics to the New York Python community at numerous meetups and PyGotham. His doctoral research at Columbia University combined the fields of 3D animation, artificial intelligence, and cinematography. Peter was first introduced to Python in 1995 and is one of the first programmers in North America to use Python for commercial product development.
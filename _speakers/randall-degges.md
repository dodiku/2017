---
name: Randall Degges
talks:
- Everything You Ever Wanted to Know About Web Authentication in Python
---

Hey! I'm Randall. I'm just a happy programmer that likes to hack stuff.

I'm currently the lead Developer Advocate at [Okta](https://www.okta.com/). I spend most of my time building open source security libraries and tools, trying to make the web a safer place. In a past life, I was the Co-Founder / CTO @ [OpenCNAM](https://www.opencnam.com/), the largest Caller ID API service.

In my free time I hack on open source projects, lift weights, and try to pet as many cute dogs as humanly possible.
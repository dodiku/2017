---
name: Alec Barrett
talks:
- Solving a crossword puzzle the hard way
---

Alec is a developer at Two-N, a data visualization firm based in New York City. He is a crossword enthusiast, and once he even participated in a crossword puzzle tournament. The project he's sharing originated during Alec's time studying at the Recurse Center, an educational programming community, where he taught himself Python.
---
title: "Announcing our third keynote: Leigh Honeywell"
date: 2017-08-31 10:00:00 -0400
image: /assets/leigh-honeywell.jpg
excerpt_separator: <!--more-->
---

Leigh Honeywell is a Technology Fellow at the ACLU. Prior to the ACLU, she
worked at Slack, Salesforce.com, Microsoft, and Symantec.<!--more--> She has
co-founded two hackerspaces, and is an advisor to several nonprofits and
startups. Leigh has a Bachelors of Science from the University of Toronto where
she majored in Computer Science and Equity Studies.

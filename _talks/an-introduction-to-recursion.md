---
abstract: Many interviews for software engineering positions include questions on
  computer science theory. Recursion is one topic that is often covered in these interviews.
  The talk will introduce recursion in a simple and detailed manner and will end with
  a coding problem from a real interview I failed!
level: Beginner
speakers:
- Rinita Gulliani
title: An Introduction to Recursion
---

For better or for worse, many software developers will encounter technical interviews on computer science theory at some point in their careers. I have known developers who avoid companies who use this interview format because they do not feel capable of passing the interview. Recursion is one of the technical interview topics that many people struggle with. This talk is targeted at beginners in hopes of showing them that recursion is learnable before they write off their ability to pass a technical interview. The talk assumes minimal knowledge of Python and programming. If you are familiar with conditionals and loops (if-statements and for-loops), you should be able to understand this talk.
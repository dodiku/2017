---
abstract: Intellectual property is everywhere in software, especially in open source.
  From licenses to copyrights, a working knowledge of IP law can help you navigate
  the FOSS world more effectively as either a developer or user.
level: Beginner
speakers:
- Noah Kantrowitz
title: Intellectual Property for Engineers
---

Intellectual property is everywhere in software, especially in open source. From licenses to copyrights, our world is one drenched in IP. This talk will cover the essentials of IP law in the US, giving both open source developers and users a working knowledge of the systems behind those big blocks of legal text. Starting from the four main branches of IP, we'll cover topics like copyrights, licenses, trademarks, CLAs, and more.
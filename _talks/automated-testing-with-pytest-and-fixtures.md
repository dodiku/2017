---
abstract: 176 functional tests. 133 unit tests. 5 browsers. How to keep test runs
  organized? Pytest and fixtures!
level: All
speakers:
- Steven Saporta
title: Automated testing with pytest and fixtures
---

Pytest is a free Python package that makes writing and running tests for your Python programs easier. Why use Pytest?
- Automatic test discovery finds and executes tests based on the names of files and functions.
- Multiple tests can run in parallel, saving time.
- It's easy to group your tests and run just the relevant ones.
- Testing that your code has raised and expected exception is easy.
- Pytest "fixtures" are a powerful tool for:
    - Setting up and tearing down objects or data required by your tests
    - Parameterizing tests to run with different environments, inputs, etc.

This talk is ideal for a moderately technical audience. Basic knowledge of Python programming will be helpful, but is not required.
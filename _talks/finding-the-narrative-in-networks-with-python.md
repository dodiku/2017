---
abstract: 'I will demonstrate how taking the view of communities of people, dolphins,
  bacteria and financial systems as networks can help us understand them better. This
  will be an interactive coding session where we will graph and analyze networks using
  Networkx and identify the stories that they tell.

  '
level: All
speakers:
- Leah Guthrie
title: Finding the narrative in networks with python
---

I will demonstrate how taking the view of communities of people, dolphins, bacteria and financial systems as networks can help us understand them better. This will be an interactive coding session where we will graph and analyze networks using Networkx and identify the stories that they tell. We will go over solutions for two common pitfalls relating to (1) computation time and memory and (2) network visualization (avoiding visualizing hair balls). I will also identify publicly available databases as well as python packages that may be useful for generating network data on your own. This talk will end by highlighting the ways that analyzing networks has a utility ranging from a personal practice for reflecting on your own social networks to a citizen scientist endeavor.
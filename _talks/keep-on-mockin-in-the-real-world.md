---
abstract: This talk is an example-led romp through some of the trickier aspects of
  mocking and patching. It picks up where a beginner's guide leaves off and looks
  at the sort of problems you are likely to face when writing tests for the kind of
  real code that is currently in your codebase.
level: Beginner
speakers:
- Andrew Burrows
title: Keep On Mockin' In The Real World
---

This talk is an example-led romp through some of the trickier aspects of mocking and patching. It picks up where a beginner's guide leaves off and looks at the sort of problems you are likely to face when writing tests for the kind of real code that is currently in your codebase. Based on years of experience teaching and mentoring developers in my workplace on testing and mocking techniques I've collected together many of the follow up questions and war stories people have brought to me after hitting problems using mocks in their tests. This talk will equip you with a set of tools, tips and tricks you can use in your own testing and a greater appreciation of what kind of code might make life difficult when mocking.
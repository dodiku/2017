---
abstract: Your team's code review practices cause ripple effects far into the future.
  In this play, see several ways a single code review can go, then fast-forward and
  rewind to see the effects -- on codebase and culture -- of different code review
  approaches.
level: Advanced
speakers:
- Sumana Harihareswara
- Jason Owen
title: Code Review, Forwards and Back
---

The setting: an office conference room. The characters: a developer, who's written a chunk of new Python code, and a team lead, who's about to review it. You'll see the code. It's not great.

What happens if the reviewer waves it through, or lets conflict aversion get the best of them? What if the reviewer says it should be "better" but doesn't articulate how? What if the review is abrasive, or nitpicky, or laid-back? What if the reviewer rewrites the code right there and then? And if we fast-forward to the same team years later, how has this code reviewing style affected the quality and evolution of the codebase, and the team's culture, skill and sustainability?

See a fast-paced montage of ways things can go. Recognize patterns from your past and present. Learn scripts for phrasing criticism constructively. And laugh.
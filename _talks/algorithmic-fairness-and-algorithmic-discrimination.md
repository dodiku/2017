---
abstract: Data is a reflection of our society, and as such, it contains the biases
  and inequalities present within. In this talk, I present the idea of algorithmic
  fairness and  various metrics for measuring the level of discrimination in a model
  and statistical methods for mitigating the effects of bias.
level: Intermediate
speakers:
- Manojit Nandi
title: Algorithmic fairness and algorithmic discrimination
---

As data-driven models are more commonly used in decision-making and public policy, we as data practitioners must be aware of the systematic biases present in our data, so we do not discriminate or reinforce vicious cycles against vulnerable groups.  I will explain the concept of algorithmic fairness and how it relates to the traditional view of machine learning classifiers. I will discuss ways to measure the extent to which a classifier discriminates against a particular minority group and showcase special algorithms for mitigating the level of disparate impact of a classifier. At the end of the talk, I will point interested audience members to resources where they can learn more about emerging trends in this topic.
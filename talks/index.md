---
title: Accepted Talks
---

{% for talk in site.talks %}
  <ol class="talks">
    <li>
      <h3><a href="{{ talk.url }}">{{ talk.title }}</a></h3>
      <h4>{{ talk.speakers | join: ', ' }}</h4>

      {{ talk.abstract | markdownify }}
    </li>
  </ol>
{% endfor %}
